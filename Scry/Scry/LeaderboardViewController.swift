//
//  LeaderboardViewController.swift
//  Scry
//
//  Created by Jordan Hilder on 4/28/21.
//

import UIKit
import Firebase
import FirebaseAuth

class LeaderboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var leaderboardTableView: UITableView!
    
    // Fixes race conditions
    let dispatchGroup = DispatchGroup()
    
    var leaderboardDisplayList: [DisplayUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Take the latest ratings from firebase and rank all the Users.
        
        leaderboardTableView.delegate = self
        leaderboardTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        populateLeaderboard()
    }

    func populateLeaderboard() {
        DispatchQueue.global(qos: .userInitiated).async {
            let downloadGroup = DispatchGroup()
            downloadGroup.enter()
            // get next refresh ready
            self.leaderboardDisplayList.removeAll()
            db.collection("users").getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    DispatchQueue.main.async {
                        for document in querySnapshot!.documents {
                            let tempItem = DisplayUser(userName: document.data()["userName"] as! String, totalPoints: document.data()["totalPoints"] as! Int, trophy: "unplaced")

                            self.leaderboardDisplayList.append(tempItem)
                        }
                        self.leaderboardDisplayList.sort { $0.totalPoints > $1.totalPoints }
                        self.assignRankings()
                        downloadGroup.wait()
                        // use for testing
                        //print("leaderboard list 1 ", self.leaderboardDisplayList)
                        self.leaderboardTableView.reloadData()
                    }
                }
            }
            downloadGroup.leave()
            //print("leaderboard list 2 ", self.leaderboardDisplayList)
            downloadGroup.wait()
        }
    }
    
    func assignRankings() {
        for index in 0..<self.leaderboardDisplayList.count {
            if(index == 0) {
                self.leaderboardDisplayList[index].trophy = "first"
            }
            else if(index == 1) {
                self.leaderboardDisplayList[index].trophy = "second"

            }
            else if(index == 2) {
                self.leaderboardDisplayList[index].trophy = "third"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        leaderboardDisplayList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath as IndexPath) as! CustomLeaderboardViewCell
        let row = indexPath.row
        cell.usernameLabel?.text = leaderboardDisplayList[row].userName
        cell.totalPointsLabel?.text = String (leaderboardDisplayList[row].totalPoints)
        cell.trophyImageView?.image = UIImage(named: leaderboardDisplayList[row].trophy)
        return cell
    }
    
}

struct DisplayUser {
    var userName: String
    var totalPoints: Int
    var trophy: String
}

class CustomLeaderboardViewCell: UITableViewCell{
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var totalPointsLabel: UILabel!
    
    @IBOutlet weak var trophyImageView: UIImageView!
}
