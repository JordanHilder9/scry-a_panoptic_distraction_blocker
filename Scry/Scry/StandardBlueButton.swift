//
//  StandardBlueButton.swift
//  Scry
//
//  Created by Jordan Hilder on 4/6/21.
//

import UIKit

class StandardBlueButton: UIButton {

    override func didMoveToWindow() {
//        self.backgroundColor = UIColor.init(red: 46/255, green: 238/255, blue: 146/255, alpha: 1)
        self.backgroundColor = UIColor.blue
        self.layer.cornerRadius = self.frame.height / 2
        self.setTitleColor(UIColor.white, for: .normal)
        // Do any additional setup after loading the view.
    }

    override func awakeFromNib() {
            super.awakeFromNib()

            layer.shadowRadius = 5.0

            // autolayout solution
            self.translatesAutoresizingMaskIntoConstraints = false
            self.widthAnchor.constraint(equalToConstant: 200).isActive = true
            self.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
}
