//
//  FocusViewController.swift
//  Scry
//
//  Created by Jordan Hilder on 4/6/21.
//

import UIKit
import UserNotifications
import CoreMotion
import FirebaseAuth
import FirebaseFirestore

protocol ChecklistUpdater{
    func incrementTotalDistractions()
}

class FocusViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var timerDisplay: UILabel!
    
    @IBOutlet weak var focusButton: UIButton!
        
    var timeLeft: Double = 0
    
    var startFocus: Bool = true
    
    let motionManager = CMMotionManager()

    @IBOutlet weak var encourageMessage: UILabel!
    
    @IBOutlet weak var displaySetTime: UIDatePicker!
    
    var focusMode: Bool = false
    
    var totalSessionTime: Double = 0
    
    var totalTimesDistracted: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        encourageMessage.alpha = 0.0
        // check if user leaves app
        let notificationCenter = NotificationCenter.default
            notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        // prep app for notifications
        UNUserNotificationCenter.current().delegate = self
        // not using this feature for the demo
        //trackMovement()
    }
        
    @objc func appMovedToBackground() {
        if(self.focusMode) {
            sendNot()
            self.totalTimesDistracted += 1
        }
    }
    
    @IBAction func buttonPress(_ sender: Any) {
        if(!focusMode) {
            timeLeft = displaySetTime.countDownDuration
            self.totalSessionTime = displaySetTime.countDownDuration
            timerDisplay.alpha = 1.0
            displaySetTime.alpha = 0.0
            encourageMessage.alpha = 1.0
            focusButton.setTitle( "End Session" , for: .normal )
            focusMode = true
            mainClock()
        } else {
            revertToTimer()
        }
        
    }
    
    func revertToTimer() {
        self.timerDisplay.text = "Set Timer"
        encourageMessage.alpha = 0.0
        displaySetTime.alpha = 1.0
        focusButton.setTitle( "Focus" , for: .normal )
        focusMode = false
        self.totalSessionTime = 0
        self.totalTimesDistracted = 0
    }
    
    func trackMovement() {
        // could try longer since we just want one motion
        motionManager.deviceMotionUpdateInterval = 0.1

        motionManager.startDeviceMotionUpdates(to: OperationQueue.current!) {
            (deviceMotion, error) -> Void in

            if(error == nil) {
                self.handleDeviceMotionUpdate(deviceMotion: deviceMotion!)
            } else {
                print("Error occurred")
            }
        }
    }

    
    let queue1 = DispatchQueue(label: "myQueue1", qos: .userInteractive)
    
    func mainClock() {
        // create a new thread for every timer object? then destroy when timer == 0?
        queue1.async {
            while(self.timeLeft > 0 && self.focusMode) {
                sleep(1)
                if(self.focusMode) {
                    self.timeLeft -= 1;
                    DispatchQueue.main.async {
                        let (h, m, s) = self.secondsToHoursMinutesSeconds (seconds: Int(self.timeLeft))
                        self.timerDisplay.text = ("\(h):\(m):\(s)")
                    }
                }
            }
            DispatchQueue.main.async {
                if(self.timeLeft <= 0) {
                    self.whenTimerDone()
                }
            }
        }
        
    }
    func whenTimerDone() {
        self.focusMode = false
        // Done + get points
        let alert = UIAlertController(
            title: "Congrats! You finished your Focus Session",
            message: "Your points will be added shortly",
            preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title:"OK", style:.default))
        self.present(alert, animated: true, completion: nil)

        
        // create a new Session Report object and store it to Firebase
        var currentSession = [String : Any]()

        currentSession["date"] = Date()
        currentSession["timeSpent"] = self.totalSessionTime
        currentSession["timesDistracted"] = self.totalTimesDistracted
        currentSession["grade"] = calculateSessionGrade()
        
            
        // reset screen
        revertToTimer()
        
        // update firebase
        let uid: String =  Auth.auth().currentUser!.uid
        let updateDoc = db.collection("users").document(uid)
        updateDoc.updateData([
            "totalPoints": FieldValue.increment(Int64(50-self.totalTimesDistracted)),
            "pastSessions": FieldValue.arrayUnion([currentSession])
            
        ]){ err in
            if let err = err {
                print("Error adding points and logging session: \(err)")
            } else {
                print("Points added + session logged")
            }
        }
        
    }
    
    func calculateSessionGrade() -> String {
        switch self.totalTimesDistracted {
        case 0:
            return("A+")
        case 1:
            return("A")
        case 2:
            return("A-")
        case 3:
            return("B+")
        case 4:
            return("B")
        case 5:
            return("B-")
        case 6:
            return("C+")
        case 7:
            return("C")
        case 8:
            return("C-")
        case 9:
            return("D+")
        case 10:
            return("D")
        case 11:
            return("D-")
        case _ where self.totalTimesDistracted > 11:
            return("F")
        default:
            return("Error: No Grade Assigned")
        }
    }
    
    let queue2 = DispatchQueue(label: "myQueue2", qos: .userInteractive)

    func handleDeviceMotionUpdate(deviceMotion:CMDeviceMotion) {
        queue2.async {
            let acceleration = deviceMotion.userAcceleration
            //let gravity = deviceMotion.gravity
            if(abs(acceleration.x) > 0.001 || abs(acceleration.y) > 0.001  || abs(acceleration.z) > 0.001 ) {
                print("acceleration.x", acceleration.x)
                print("acceleration.y", acceleration.y)
                print("acceleration.z", acceleration.z)
                DispatchQueue.main.async {
                    self.totalTimesDistracted += 1
                    self.encourageMessage.text = "please put phone down"
                    self.sendLocalNot(notiType: "movement")
                }
                sleep(5)
                self.encourageMessage.text = "Focus on your work"
            }
//            if(self.focusMode == true) {
//                if(gravity.x != 0 || gravity.y != 0 || gravity.z != 0) {
//                    print("grav.x", gravity.x)
//                    print("grav.y", gravity.y)
//                    print("grav.z", gravity.z)
//                    DispatchQueue.main.async {
//                        self.totalTimesDistracted += 1
//                        self.encourageMessage.text = "please put phone down"
//                        self.sendLocalNot(notiType: "movement")
//                    }
//                    sleep(5)
//                    self.encourageMessage.text = "Focus on your work"
//                }
//            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func sendNot() {
        // testing method
        print("in noti method")
        // create an object that holds the data for our notification
        let notification = UNMutableNotificationContent()
        notification.title = "Please go Back to the Focus Session"
        notification.subtitle = "It's ok to be a little distracted"
        notification.body = "Complete your session"
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        // set up a request to tell iOS to submit the notification with that trigger
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: notification,
            trigger: notificationTrigger)
        
        // testing statement
        print("completed")
        // submit the request to iOS
        UNUserNotificationCenter.current().add(request) {
            (error) in
            print("Request error: ", error as Any)
        }
    }
    
    func sendLocalNot(notiType: String) {
        // create an object that holds the data for our notification
        let notification = UNMutableNotificationContent()
        if(notiType == "switchScreens") {
            notification.title = "Please go Back to the Focus Session"
            notification.subtitle = "It's ok to be a little distracted"
            notification.body = "Complete your session"
        }
        else {
            notification.title = "Put the Phone Down"
            notification.subtitle = "It's ok to be a little distracted"
            notification.body = "Complete your session"
        }
        
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        // set up a request to tell iOS to submit the notification with that trigger
        let request = UNNotificationRequest(
            identifier: UUID().uuidString,
            content: notification,
            trigger: notificationTrigger)
        
        // testing statement
        print("completed")
        // submit the request to iOS
        UNUserNotificationCenter.current().add(request) {
            (error) in
            print("Request error: ", error as Any)
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.focusMode) {
            sendLocalNot(notiType: "switchScreens")
            self.totalTimesDistracted += 1
        }
    }
    
}
