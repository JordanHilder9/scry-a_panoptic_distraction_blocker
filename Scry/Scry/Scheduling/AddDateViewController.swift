//
//  AddDateViewController.swift
//  Scry
//
//  Created by Jordan Hilder on 3/29/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class AddDateViewController: UIViewController {
    
    var delegate: ScheduleViewController = ScheduleViewController()
    var currentAppointment = [String : Any]()
    var uid: String = ""
    
    @IBOutlet weak var createAppButton: StandardBlueButton!
    @IBOutlet weak var appointmentName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uid =  Auth.auth().currentUser!.uid
        
    }
    
    @IBOutlet weak var calObj: UIDatePicker!
    
    
    @IBAction func buttonPress(_ sender: Any) {
        // if the appointmentName is blank, error check
        if(appointmentName.text! == "") {
            let alert = UIAlertController(
                title: "Cannot Create Appointment",
                message: "no text inputted",
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title:"OK",style:.default))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            // first put date into appointment class
            currentAppointment["appName"] = appointmentName.text!
            currentAppointment["storedDate"] = calObj.date
            let formatter1 = DateFormatter()
            formatter1.dateStyle = .short
            let dateDisplay = formatter1.string(from: currentAppointment["storedDate"] as! Date)
            currentAppointment["stringDate"] = dateDisplay
            // get reference to user
            let updateDoc = db.collection("users").document(uid)
            updateDoc.updateData([
                "appointments": FieldValue.arrayUnion([currentAppointment])
            ])
            let prevVC = delegate as AppToTable
            let tempApp = DisplayAppointment(appName: appointmentName.text!, stringDate: dateDisplay)
            prevVC.addAppointmentToTable(currApp: tempApp)
            prepNotification()
        }
    }
    
    func prepNotification() {
        let content = UNMutableNotificationContent()
        content.title = appointmentName.text!
        content.body = "Enter Scry To Start Your Focus Session 👁"
        
        // Do any additional setup after loading the view.
        // Configure the recurring date.
        
        let dateComponents = Calendar.current.dateComponents([.month,  .day, .hour, .minute], from: calObj.date)
        print("date comps ", dateComponents)
        
        // Create the trigger as a repeating event.
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: dateComponents, repeats: false)
        
        // Create the request
        let uuidString = UUID().uuidString
        print("uuidString", uuidString)
        let request = UNNotificationRequest(identifier: uuidString,
                                            content: content, trigger: trigger)
        
        // Schedule the request with the system.
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.add(request) { (error) in
            if error != nil {
                // Handle any errors.
            }
        }
    }
    
    // code to enable tapping on the background to remove software keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

struct DisplayAppointment {
    var appName: String
    // defaut value
    //var storedDate: Date = Date(timeIntervalSinceReferenceDate: -123456789.0)
    var stringDate: String
}
