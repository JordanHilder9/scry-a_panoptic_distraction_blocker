//
//  ScheduleViewController.swift
//  Scry
//
//  Created by Jordan Hilder on 3/29/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

protocol AppToTable {
    func addAppointmentToTable(currApp:DisplayAppointment)
}

class ScheduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AppToTable {

    var appList:[DisplayAppointment] = []
    
    // May need to delete these two
    var currUser = [String: Any]()
    var firebaseAppointments = [String: Any]()

    @IBOutlet weak var appTableView: UITableView!
    
    let dispatchGroup = DispatchGroup()
    var uid: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        appTableView.delegate = self
        appTableView.dataSource = self
        uid =  Auth.auth().currentUser!.uid

        // get appointments from firestore
        updateAppList()
        //dispatchGroup.notify(queue: .main) {self.appTableView.reloadData()}
        dispatchGroup.notify(queue: DispatchQueue.main, execute: {self.appTableView.reloadData()})
        //print("appList is empty? ", self.appList.isEmpty)
        
    }
    
    func updateAppList() {
        dispatchGroup.enter()
        db.collection("users").document(uid).getDocument { (document, error) in
            let appArray = document?.data()!["appointments"]! as! [Any]
            
            for apps in 0 ..< appArray.count {
                let currentAppointment = appArray[apps] as! Dictionary<String, AnyObject>
                let tempApp = DisplayAppointment(appName: currentAppointment["appName"] as! String, stringDate: currentAppointment["stringDate"] as! String)
                //print("tempApp \(apps) ", tempApp)
                self.appList.append(tempApp)
                self.appTableView.reloadData()
            }
            // testing line
            //print("appList is empty 2? ", self.appList)
        }
        dispatchGroup.leave()
        //dispatchGroup.notify(queue: DispatchQueue.main, execute: {self.appTableView.reloadData()})
    }

    @IBAction func deleteAllItemsButton(_ sender: Any) {
        let controller = UIAlertController(
            title: "Delete All Appointments",
            message: "This Cannot be Undone",
            preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(
                                title: "Ok",
                                style: .default,
                                handler: {(action) in self.deleteItemsInDatabase()}))
        controller.addAction(UIAlertAction(
                                title: "Cancel",
                                style: .default,
                                handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    func deleteItemsInDatabase() {
        let updateDoc = db.collection("users").document(uid)
        db.collection("users").document(uid).getDocument { (document, error) in
            let appArray = document?.data()!["appointments"]! as! [Any]
            for items in 0 ..< appArray.count {
                updateDoc.updateData([
                    "appointments": FieldValue.arrayRemove([appArray[items]])
                ]){ err in
                    if let err = err {
                        print("Error deleting document: \(err)")
                    } else {
                        print("Deleted All Appointments")
                    }
            }
            }
        }
        self.appList.removeAll()
        self.appTableView.reloadData()    }
    
    // Adds current app to table
    func addAppointmentToTable(currApp:DisplayAppointment) {
        let tempApp = DisplayAppointment(appName: currApp.appName, stringDate: currApp.stringDate)
        appList.append(tempApp)
        self.appTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAppointment",
           let addDateVC = segue.destination as? AddDateViewController {
            addDateVC.delegate = self
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        appList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appTableView.dequeueReusableCell(withIdentifier: "AppTableViewCell", for: indexPath as IndexPath) as! CustomAppViewCell
        let row = indexPath.row
        //print("Appointment: " + appList[row].appName)
        //print("Time: " + appList[row].stringDate)
        cell.displayLabelApp?.text = "Appointment: " + appList[row].appName
        cell.displayLabelDate?.text = "Time: " + appList[row].stringDate
        return cell
    }
}

class CustomAppViewCell: UITableViewCell {
    @IBOutlet weak var displayLabelApp: UILabel!
    
    @IBOutlet weak var displayLabelDate: UILabel!
}


