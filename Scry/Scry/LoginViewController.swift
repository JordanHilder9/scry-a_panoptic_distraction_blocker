//
//  LoginViewController.swift
//  Scry
//
//  Created by Jordan Hilder on 3/29/21.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

public let db = Firestore.firestore()

class LoginViewController: UIViewController {
    
    @IBOutlet weak var segCtrl: UISegmentedControl!
    
    @IBOutlet weak var textFieldLoginEmail: UITextField!
    
    @IBOutlet weak var textFieldLoginPassword: UITextField!
    
    @IBOutlet weak var confirmLable: UILabel!
    
    @IBOutlet weak var confirmTextField: UITextField!
    
    @IBOutlet weak var signButton: UIButton!
    
    @IBOutlet weak var statusLable: UILabel!
    // Determines if segue to main vc should be successful
    var badParameters:Bool = true
    
    // tells program what state seg ctrl in
    var loginState = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusLable.text! = ""
        //set background
        // makes sure the button press will do a sign in by default
        loginState = true
        // make sure these labels start off hidden
        confirmLable.isHidden = true
        confirmTextField.isHidden = true
        
        // automatically accepts user
        Auth.auth().addStateDidChangeListener() {
            auth, user in
            
            if user != nil {
                //        self.performSegue(withIdentifier: "toHomeScreen", sender: nil)
                self.textFieldLoginEmail.text = nil
                self.textFieldLoginPassword.text = nil
            }
        }
        //Auth.auth().signOut()
    }
    
    
    @IBAction func buttonTouch(_ sender: Any) {
        // If the seg ctrl on login, do the login stuff
        if(loginState){
            loginDidTouch()
        }
        // else do the button press for sign up
        else {
            signUpDidTouch()
        }
    }
    
    func loginDidTouch() {
        guard let email = textFieldLoginEmail.text,
              let password = textFieldLoginPassword.text,
              email.count > 0,
              password.count > 0
        else {
            let alert = UIAlertController(
                title: "Sign in failed",
                message: "no text inputted",
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title:"OK",style:.default))
            self.present(alert, animated: true, completion: nil)
            self.statusLable.text! = "Sign In Failure"
            return
        }
        let propEmail: String = email + "@gmail.com"
        Auth.auth().signIn(withEmail: propEmail, password: password) {
            user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(
                    title: "Sign In failed",
                    message: error.localizedDescription,
                    preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title:"OK",style:.default))
                self.present(alert, animated: true, completion: nil)
                self.statusLable.text! = "Sign In Failure"
            }
            else {
                self.performSegue(withIdentifier: "toScheduleScreen", sender: nil)
            }
        }
        
    }
    
    // comes to this function if the seg ctrl is in signup mode
    func signUpDidTouch() {
        let userName: String = self.textFieldLoginEmail.text!
        let propEmail: String = userName + "@gmail.com"
        if(textFieldLoginPassword.text! == confirmTextField.text!) {
            Auth.auth().createUser(withEmail: propEmail, password: textFieldLoginPassword.text!) { user, error in
                if error == nil {
                    Auth.auth().signIn(withEmail: (propEmail),
                                       password: self.textFieldLoginPassword.text!)
                    // Add a new document with a generated id.
                    let userID = Auth.auth().currentUser!.uid
                    db.collection("users").document(userID).setData( [
                        "totalPoints": 0,
                        "userName" : userName,
                        "appointments" : [],
                        "pastSessions" : []
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            //print("Document added with ID: \(documentID)")
                        }
                        self.performSegue(withIdentifier: "toScheduleScreen", sender: nil)
                        self.statusLable.text! = "Success"
                    }
                } else {
                    //Set label to unsucessful
                    self.statusLable.text! = "Sign Up Failure"
                }
            }
        }
    }
    
    @IBAction func segChange(_ sender: Any) {
        switch segCtrl.selectedSegmentIndex {
        // make user registration invis -  confirm label and text field
        case 0:
            loginState = true
            confirmLable.isHidden = true
            confirmTextField.isHidden = true
            signButton.setTitle("Sign In", for: .normal)
        case 1:
            loginState = false
            confirmLable.isHidden = false
            confirmTextField.isHidden = false
            signButton.setTitle("Sign Up", for: .normal)
        default:
            loginState = true
            confirmLable.isHidden = true
            confirmTextField.isHidden = true
            signButton.setTitle("Sign In", for: .normal)
        }
    }
    // code to enable tapping on the background to remove software keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

